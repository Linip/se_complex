CC = g++
LD = g++
CCFLAGS = -g -Wall -pedantic -std=c++17
LDFLAGS =

CPP = $(wildcard *.cpp)
OBJ = $(subst .cpp,.o,$(CPP))

.SUFFIXES: .cpp .o
.PHONY: all clean

PROG = testcmp.out

BIN_PATH = bin
OBJ_PATH = bin/obj

OBJ_WITH_PATHS = $(addprefix $(OBJ_PATH)/, $(OBJ))
BIN_WITH_PATH = $(addprefix $(BIN_PATH)/, $(PROG))

$(BIN_WITH_PATH): $(OBJ_WITH_PATHS)
	$(LD) $(LDFLAGS) -o $(BIN_WITH_PATH) $(OBJ_WITH_PATHS)
$(OBJ_PATH)/testcmp.o: testcmp.cpp mycomplex.h
	$(CC) $(CCFLAGS) -c -o $(OBJ_PATH)/testcmp.o testcmp.cpp
$(OBJ_PATH)/mycomplex.o: mycomplex.cpp mycomplex.h
	$(CC) $(CCFLAGS) -c -o $(OBJ_PATH)/mycomplex.o mycomplex.cpp

clean:
	rm -f $(OBJ_WITH_PATHS) $(BIN_WITH_PATH)