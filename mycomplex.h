#ifndef _MY_COMPLEX_H_
#define _MY_COMPLEX_H_

#include <iostream>

using namespace std;
/**
 * @class Complex
 * @brief Реализация типа данных Комплексное число
 * @details Класс реализует основные арифметические операции с комплексным чилом, ввод и вывод числа, абсолютное значение
 */
class Complex {
    double Re;
    double Im;

public:
    /**
     * @brief Конструктор класса
     * @details Инициализирует объект класса из везественной и мнимой частей числа
     * @param aRe Вещественная часть
     * @param aIm Мнимая часть
     */
    Complex(double aRe = 0, double aIm = 0);

    /**
     * @brief Конструктор класса
     * @details Инициализирует объект класса из другого объекта Соmplex
     */
    Complex(const Complex &);

    /**
     * @brief Конструктор класса
     * @details Инициализирут комплексное число с нулевым значением
     */
    ~Complex();

    /**
     * @brief Сеттер вещественной и мнимой части числа
     * @param aRe Вещественная часть
     * @param aIm Мнимая часть
     */
    void Set(double aRe, double aIm = 0);

    /**
     * @brief Перегрузка приведения к типу double
     * @return Абсолютное значение комплексного числа
     */
    operator double();

    /**
     * @brief Абсолютное значение комплексного числа
     * @return Абсолютное значение комплексного числа
     */
    double abs();

    /**
     * @brief Перегружает попток ввода
     * @return Поток вывода
     */
    friend istream &operator>>(istream &, Complex &);

    /**
     * @brief Перегружает попток вывода
     * @return Поток ввода
     */
    friend ostream &operator<<(ostream &, Complex &);

    /**
     * @brief Перегрузка оператора сложения
     * @param aRval
     * @return Complex
     */
    Complex operator+(const Complex &);

    /**
     * @brief Перегрузка оператора вычитания
     * @param aRval Комплексное число
     * @return Complex
     */
    Complex operator-(const Complex &);

    /**
     * @brief Перегрузка оператора сложения
     * @param aRval Вещественная часть числа
     * @return Complex
     */
    Complex operator+(const double &);

    /**
     * @brief Перегрузка оператора сложения
     * @param aLval Вещественное число
     * @param aRval Комплексное число
     * @return Complex
     */
    friend Complex operator+(const double &, const Complex &);

    /**
     * @brief Перегрузка оператора вычитания
     * @param aRval Вещественное число
     * @return Complex
     */
    Complex operator-(const double &);

    /**
     * @brief Перегрузка оператора вычитания
     * @param aLval Вещественное число число
     * @param aRval Комплексное число
     * @return Complex
     */
    friend Complex operator-(const double &, const Complex &);

    /**
     * @brief Перегрузка оператора умножения
     * @param aRval Комплексное число
     * @return Complex
     */
    Complex operator*(const Complex &);

    /**
     * @brief Перегрузка оператора умножения
     * @param aRval Вещественное число
     * @return Complex
     */
    Complex operator*(const double &);

    /**
     * @brief Перегрузка оператора умножения
     * @param aLval Вещественное число
     * @param aRval Комплексное число
     * @return Complex
     */
    friend Complex operator*(const double &, const Complex &);

    /**
     * @brief Перегрузка оператора деления
     * @param aRval Вещественное число
     * @return Complex
     */
    Complex operator/(const double &);

    /**
     * @brief Перегрузка оператора присваивания с пирибавлением
     * @param aRval Комплексное число
     * @return Complex
     */
    Complex &operator+=(const Complex &);

    /**
     * @brief Перегрузка оператора писваивания с вычитанием
     * @param aRval Комплексное число
     * @return Complex
     */
    Complex &operator-=(const Complex &);

    /**
     * @brief Перегрузка оператора присваивания с умножением
     * @param aRval Комплексное число
     * @return Complex
     */
    Complex &operator*=(const Complex &);

    /**
     * @brief Перегрузка оператора присваивания с пирибавлением
     * @param aRval Вещественное число
     * @return Complex
     */
    Complex &operator+=(const double &);

    /**
     * @brief Перегрузка оператора присваивания с вычитанием
     * @param aRval Вещественное число
     * @return Complex
     */
    Complex &operator-=(const double &);

    /**
     * @brief  Перегрузка оператора присваивания с умножением
     * @param aRval Вещественное число
     * @return Complex
     */
    Complex &operator*=(const double &);

    /**
     * @brief Перегрузка оператора присваивания с делением
     * @param aRval Вещественное число
     * @return Complex
     */
    Complex &operator/=(const double &);

    /**
     * @brief Перегрузка оператора присваивания
     * @param aRval Комплексное число
     * @return Complex
     */
    Complex &operator=(const Complex &);

    /**
     * @brief Перегрузка оператора присваивания
     * @param aRval Вещественное число
     * @return Complex
     */
    Complex &operator=(const double &);
};

#endif
